importlib_metadata~=4.11.4
beautifulsoup4~=4.11.1
sphinx~=5.3.0
pytest~=7.1.3
setuptools~=65.5.0