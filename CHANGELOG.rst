=========
Changelog
=========

Version 0.4.1
=============
- executable heet nu htmlcleaner
- unit tests toegevoegd


Version 0.3.1
=============
- update naar pyscaffold 4

Version 0.3
===========

- verbeterd schoonmaakalgorithme

Version 0.2
===========

- latexml2sitescore werkt
- overwrite optie toegevoegd
